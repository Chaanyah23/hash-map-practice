import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        Map<String, String> hashmap = new HashMap<>();

        hashmap.put("Milk", "Dairy");
        hashmap.put("Apples", "Fruit");
        hashmap.put("Cucumber","Vegtables");
        hashmap.put("Chicken","Meat");
        hashmap.put("Bread","Protein");

        System.out.println(hashmap.get("Bread"));
        System.out.println(hashmap);

        System.out.println(hashmap);

        HashMap<String, Integer> hashmap2 = new HashMap<>();

        hashmap2.put("Chaan", 77);
        hashmap2.put("Dan", 41);
        hashmap2.put("Terrance", 65);
        hashmap2.put("Arnold", 10);

        System.out.println(hashmap2);
        System.out.println(hashmap2.get("Chaan"));

    }

}
